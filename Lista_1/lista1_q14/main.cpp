#include <iostream>
#include <math.h>

using namespace std;

int main() {
    int hipotenusa;
    int catetoOposto;
    int catetoAdjascente;

    int variavelDeTroca;

    cout << "Digite 3 numeros: ";
    cin >> hipotenusa;
    cin >> catetoOposto;
    cin >> catetoAdjascente;

    if(catetoAdjascente > hipotenusa) {
        variavelDeTroca = hipotenusa;
        hipotenusa = catetoAdjascente;
        catetoAdjascente = variavelDeTroca;
    }

    if(catetoOposto > hipotenusa) {
        variavelDeTroca = hipotenusa;
        hipotenusa = catetoOposto;
        catetoOposto = variavelDeTroca;
    }

    if(pow(hipotenusa, 2) == pow(catetoAdjascente, 2) + pow(catetoOposto, 2)) {
        cout << hipotenusa << "² = " << catetoAdjascente << "² + " << catetoOposto << "²" << endl;
        cout << "Os números formam um triangulo retângulo.";
    } else {
        cout << "Os números não formam um triânguo retângulo";
    }

    return 0;
}