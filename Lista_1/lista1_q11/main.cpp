#include <iostream>

using namespace std;

int main() {

    int numero;
    int isPrimo = true;

    cout << "Digite um número e iremos verificar se ele é primo: ";
    cin >> numero;

    if(numero <= 1 || numero % 2 == 0) {
        isPrimo = false;
    }

    for(int i = 3; i <= numero/2; i += 2 ) {
        if(numero % i == 0) {
            isPrimo = false;
        }
    }

    cout << "O número " << numero << (isPrimo ? " é primo" : " não é primo");

    return 0;
}