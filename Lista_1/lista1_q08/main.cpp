#include <iostream>

using namespace std;

int fatorial(int numero) {
    if (numero == 0)
        return 0;
    else if (numero == 1)
        return 1;

    return numero * fatorial(numero - 1);
}

int main() {

    int numero;

    cout << "Digite um número: ";
    cin >> numero;

    cout << "O " << numero << "! é: " << fatorial(numero);

    return 0;
}