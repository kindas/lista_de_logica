#include <iostream>

using namespace std;

int main() {

    int quantidadeDeAlunos = 0;
    double *notas;

    cout << "Quantos alunos você quer cadastrar a nota: ";
    cin >> quantidadeDeAlunos;

    notas = (double *) malloc(quantidadeDeAlunos * sizeof(int));

    for (int i = 0; i < quantidadeDeAlunos; i++) {
        cout << "Digite a nota do aluno " << i + 1 << ": ";
        cin >> notas[i];
    }

    double maiorNota = notas[0];
    double menorNota = notas[0];
    for (int i = 0; i < quantidadeDeAlunos; i++) {
        if (notas[i] > maiorNota) {
            maiorNota = notas[i];
        }
        if (notas[i] < menorNota) {
            menorNota = notas[i];
        }
    }

    cout << "A menor nota é: " << menorNota << endl;
    cout << "A maior nota é: " << maiorNota << endl;

    return 0;
}