#include <iostream>
#include <math.h>

using namespace std;

int main() {

    int quantidadeDeNumeros;
    int *numeros;

    cout << "Quantos numeros deseja calcular o quadrado? ";
    cin >> quantidadeDeNumeros;

    numeros = (int*) malloc(quantidadeDeNumeros * sizeof(int));

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << "Digite o numero " << i + 1 << ": ";
        cin >> numeros[i];
    }

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << "O quadrado de " << numeros[i] << " é: " << pow(numeros[i], 2) << endl;
    }

    return 0;
}