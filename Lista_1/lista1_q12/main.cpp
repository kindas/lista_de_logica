#include <iostream>
#include <math.h>

using namespace std;

int main() {
    int numeroBinario;
    cout << "Digite um número em binario: ";
    cin >> numeroBinario;

    int resto = 0;
    int potencia = 0;
    int numeroDecimal = 0;
    do {
        resto %= 10;
        numeroDecimal += resto * pow(2, potencia);
        numeroBinario /= 10;
        potencia++;
    } while (numeroBinario != 0);

    cout << "O número em base decimal é: " << numeroDecimal;

    return 0;
}