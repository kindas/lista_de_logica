#include <iostream>

using namespace std;

int main() {

    int quantidadeDeDias;
    int *quantidadeDeDiscos;

    cout << "Quantos dias deseja cadastrar? ";
    cin >> quantidadeDeDias;

    quantidadeDeDiscos = (int*) malloc(quantidadeDeDias * sizeof(int));

    for (int i = 0; i < quantidadeDeDias ; i++) {
        cout << "Digite a quantidade de discos para o dia " << i + 1 << ": ";
        cin >> quantidadeDeDiscos[i];
    }

    int maiorDia = 0;
    for (int i = 0; i < quantidadeDeDias; i++) {
        if(quantidadeDeDiscos[i] > quantidadeDeDiscos[maiorDia]) {
            maiorDia = i;
        }
    }

    cout << "O dia de maior vendas foi o " << maiorDia + 1 << ", com " << quantidadeDeDiscos[maiorDia] << " discos vendidos";

    return 0;
}