#include <iostream>

using namespace std;

int main() {

    int numeroDecimal;
    int numeroBinario = 0;
    int potencia = 1;

    cout << "Dite um número em base decimal: ";
    cin >> numeroDecimal;


    int numeroAuxiliar = numeroDecimal;
    int resto;

    do {
        resto = numeroAuxiliar % 2;
        numeroAuxiliar /= 2;
        numeroBinario = numeroBinario + resto * potencia;
        potencia *= 10;
    } while (numeroAuxiliar > 0);

    cout << "O número em binário é: " << numeroBinario;

    return 0;
}