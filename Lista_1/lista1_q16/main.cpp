#include <iostream>

using namespace std;

int main() {
    int numero;
    int isPalindromo = false;
    cout << "Digite um numero com quantidade par de algarismos: ";
    cin >> numero;

    int numeroApoio = numero;
    int qntDeAlgarismos = 0;
    while(numeroApoio != 0) {
        numeroApoio /= 10;
        qntDeAlgarismos++;
    }

    if(qntDeAlgarismos % 2 != 0) {
        cout << "O número deve conter uma quantidade par de algarismos!!!";
        return 0;
    }

    int metadeInvertida = 0;
    for (int i = 0; i < qntDeAlgarismos / 2; i++) {
        metadeInvertida = metadeInvertida * 10 + numero % 10;
        numero /= 10;
    }

    if(numero == metadeInvertida){
        cout << "É palindromo" << endl;
    } else {
        cout << "Não é palindromo" << endl;
    }

    
    return 0;
}