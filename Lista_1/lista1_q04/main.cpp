#include <iostream>

using namespace std;

int main() {
    int base;
    int expoente;
    int potencia = 1;

    cout << "Digite a base: ";
    cin >> base;
    cout << "Digite o expoente: ";
    cin >> expoente;

    cout << base << " elevado a " << expoente << endl;

    int contador = 1;
    while(contador <= expoente) {
        potencia *= base;
        contador++;
    }

    cout << "O Quadrado de " << base << " é: " << potencia << endl;


    return 0;
}