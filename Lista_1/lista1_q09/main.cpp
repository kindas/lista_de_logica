#include <iostream>

using namespace std;

int main() {

    int numero;
    int i, j;

    cout << "Digite um número: ";
    cin >> numero;
    cout << "Digite outro número: ";
    cin >> i;
    cout << "Digite mais um número: ";
    cin >> j;

    int contador = 0;
    int multiplo = 0;

    cout << "Os " << numero << " primeiros multiplos de " << i << " e " << j << "são: ";
    while(contador < numero) {
        if(multiplo % i == 0 || multiplo % j == 0) {
            if(contador != 0) {
                cout << ", ";
            }

            cout << multiplo;
            contador++;
        }
        multiplo++;
    }


    return 0;
}