#include <iostream>

using namespace std;

int main() {

    int numero;
    cout << "Dite um número inteiro positivo";
    cin >> numero;

    int i = 1;
    int contador = 1;
    cout << "Os " << numero << " primeiros números impares são: ";
    while(contador <= numero) {
        cout << i << ", ";
        i += 2;
        contador++;
    }

    return 0;
}