#include <iostream>

using namespace std;

int main() {
    int quantidadeDeNumeros;
    int *numeros;

    cout << "Qual o tamanho da sequencia de numeros? ";
    cin >> quantidadeDeNumeros;

    numeros = (int *) malloc(quantidadeDeNumeros * sizeof(int));

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << "Digite o " << i + 1 << " número: ";
        cin >> numeros[i];
    }

    int anterior = 0;
    int sequencia = 0;
    int maiorSequencia = 1;
    int atual = 0;

    for (int i = 0; i < quantidadeDeNumeros; ++i) {
        atual = numeros[i];
        if (anterior < atual) {
            sequencia++;
            if (sequencia > maiorSequencia) {
                maiorSequencia = sequencia;
            }
        } else {
            sequencia = 1;
        }
        anterior = atual;
    }

    cout << "A maior sequencia crescente de números é: " << maiorSequencia;

    return 0;
}