#include <iostream>

using namespace std;

int main() {

    int numero;
    int somatorio = 0;
    cout << "Digite um inteiro positivo";
    cin >> numero;

    int i = 1;
    while(i <= numero) {
        somatorio += i;
        i++;
    }

    cout << "O somatório dos " << numero << " primeiros números é: " << somatorio;

    return 0;
}