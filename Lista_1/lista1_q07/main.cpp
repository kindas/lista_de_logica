#include <iostream>

using namespace std;

int main() {

    int quantidadeDeNumeros;
    int *numeros;
    int somatorio = 0;

    cout << "Quantos números deseja inserir: ";
    cin >> quantidadeDeNumeros;

    numeros = (int *) malloc(quantidadeDeNumeros * sizeof(int));

    for(int i = 0; i < quantidadeDeNumeros; i++) {
        cout << "Digite o " << i + 1 << " número: ";
        cin >> numeros[i];
    }

    cout << "A soma dos números pares em [";

    for(int i = 0; i < quantidadeDeNumeros; i++) {
        if(i != 0) {
            cout << ", ";
        }
        cout << numeros[i];
        if(numeros[i] % 2 == 0) {
            somatorio += numeros[i];
        }
    }

    cout << "] é: " << somatorio;

    return 0;
}