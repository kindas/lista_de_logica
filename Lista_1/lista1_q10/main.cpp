#include <iostream>

using namespace std;

int main() {
    int numero;
    cout << "Digite um número e iremos verificar se ele é triangular: ";
    cin >> numero;

    int contador = 0;
    int resultado = 0;
    while (resultado < numero) {
        contador++;
        resultado = contador * (contador + 1) * (contador + 2) ;
    }

    if(resultado == numero) {
        cout << numero << " é tringular: " << contador << " * " << contador +1 << " * " << contador + 2;
    } else {
        cout << numero << " não é tringular";
    }

    return 0;
}