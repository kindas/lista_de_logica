#include <iostream>

using namespace std;

int main() {
    int quantidadeDeNumeros;
    int *numeros;

    cout << "Quantidade de números da sequencia";
    cin >> quantidadeDeNumeros;

    numeros = (int *) malloc(quantidadeDeNumeros * sizeof(int));

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << " Digite o " << i + 1 << " número da sequência: ";
        cin >> numeros[i];
    }

    cout << "A ordem inversa é: [";
    for (int i = quantidadeDeNumeros - 1; i >= 0; i--) {
        cout << numeros[i];
        if (i != 0) {
            cout << ", ";
        }
    }
    cout << "]";

    return 0;
}