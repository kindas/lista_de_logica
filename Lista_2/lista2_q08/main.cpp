#include <iostream>

using namespace std;

int main() {
    int quantidadeDeNumeros;
    int *vetor1;
    int *vetor2;
    int produtoEscalar = 0;

    cout << "Digite a quantidade de numeros que o vetor possui: ";
    cin >> quantidadeDeNumeros;

    vetor1 = (int *) malloc(quantidadeDeNumeros * sizeof(int));
    vetor2 = (int *) malloc(quantidadeDeNumeros * sizeof(int));

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << "Digite o " << i + 1 << " valor do vetor1: ";
        cin >> vetor1[i];
    }

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << "Digite o " << i + 1 << " valor do vetor2: ";
        cin >> vetor2[i];

        produtoEscalar += vetor1[i] * vetor2[i];
    }

    cout << "O produto escalar dos 2 vetores é: " << produtoEscalar;

    return 0;
}