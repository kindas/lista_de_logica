#include <iostream>

using namespace std;

int main() {
    int quantidadeDeNumeros;
    int *numeros;
    int soma = 0;
    int maiorSoma = 0;
    int posicaoInicial;
    int posicaoFinal;

    cout << "Quantos numeros deseja inserir? ";
    cin >> quantidadeDeNumeros;

    numeros = (int *) malloc(quantidadeDeNumeros * sizeof(int));

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << "Digite o " << i + 1 << "numero da sequencia: ";
        cin >> numeros[i];
    }

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        soma = numeros[i];
        for (int j = i + 1; j < quantidadeDeNumeros; j++) {
            soma += numeros[j];
            if (soma > maiorSoma) {
                maiorSoma = soma;
                posicaoInicial = i;
                posicaoFinal = j;
            }
        }
    }

    cout << "A maior soma sequencial são dos elementos: [";
    for (int i = posicaoInicial; i <= posicaoFinal; i++) {
        cout << numeros[i];
        if (i != posicaoFinal) {
            cout << ", ";
        }
    }
    cout << "] = " << maiorSoma;
    return 0;
}