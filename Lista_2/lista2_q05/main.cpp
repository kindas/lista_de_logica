#include <iostream>

using namespace std;

int isPrimo(int numero) {
    int isPrimo = true;
    if (numero <= 1 || numero % 2 == 0) {
        isPrimo = false;
    }

    for (int i = 3; i <= numero / 2; i += 2) {
        if (numero % i == 0) {
            isPrimo = false;
        }
    }

    return isPrimo;
}

int main() {
    int quantidadeDeNumeros;
    int *numeros;
    int somaDosPrimos = 0;
    cout << "Quantos números deseja inserir? ";
    cin >> quantidadeDeNumeros;

    numeros = (int *) malloc(quantidadeDeNumeros * sizeof(int));

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << "Digite o " << i + 1 << " número: ";
        cin >> numeros[i];
    }

    cout << "A soma dos primos contidos em [";

    for (int i = 0; i < quantidadeDeNumeros; i++) {
        cout << numeros[i];
        if (i != quantidadeDeNumeros - 1) {
            cout << ", ";
        }
        if (isPrimo(numeros[i])) {
            somaDosPrimos += numeros[i];
        }
    }

    cout << "] é: " << somaDosPrimos;
    return 0;
}