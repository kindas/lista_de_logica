#include <iostream>

using namespace std;

int main() {
    int numero;
    int catetoAdjascente;
    int achou = false;

    cout << "Digite um numero maior que um: ";
    cin >> numero;

    for (int hipotenusa = 1; hipotenusa <= numero; hipotenusa++) {
        for (int catetoOposto = 1; catetoOposto < hipotenusa && !achou; catetoOposto++) {
            catetoAdjascente = catetoOposto;
            while (catetoOposto * catetoOposto + catetoAdjascente * catetoAdjascente < hipotenusa * hipotenusa) {
                catetoAdjascente++;
            }
            if (catetoOposto * catetoOposto + catetoAdjascente * catetoAdjascente == hipotenusa * hipotenusa) {
                achou = true;
                cout << hipotenusa << "² = " << catetoOposto << "² + " << catetoAdjascente << "²" << endl;
            }
        }
        achou = false;
    }

    return 0;
}