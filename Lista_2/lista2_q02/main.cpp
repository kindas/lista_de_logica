#include <iostream>
#include <math.h>

using namespace std;

int main() {
    int p, q;
    int quantidadeDeAlgarismosEmP = 0;
    int quantidadeDeAlgarismosEmQ = 0;
    int apoio = p;
    int isSubConjunto = false;

    cout << "Vericaremos de P é subconjunto de Q:" << endl;
    cout << "Digite valor de P: ";
    cin >> p;
    cout << "Digite valor de Q: ";
    cin >> q;

    apoio = p;
    while (apoio != 0) {
        apoio /= 10;
        quantidadeDeAlgarismosEmP++;
    }

    apoio = q;
    while (apoio != 0) {
        apoio /= 10;
        quantidadeDeAlgarismosEmQ++;
    }

    if(quantidadeDeAlgarismosEmP > quantidadeDeAlgarismosEmQ) {
        cout << "P não pode ter mais algarismos que Q";
        return 0;
    }

    apoio = q;
    int potencia = pow(10, quantidadeDeAlgarismosEmP);
    while (apoio != 0) {
        if(apoio % potencia == p) {
            isSubConjunto = true;
        }
        apoio /= 10;
    }

    if(isSubConjunto) {
        cout << p << " é subconjunto de " << q;
    } else {
        cout << p << " não é subconjunto de " << q;
    }

    return 0;
}