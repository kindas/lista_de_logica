#include <iostream>

using namespace std;

int fibonacci(int numero) {
    if(numero == 0)
        return 0;
    else if (numero == 1)
        return 1;

    return fibonacci(numero - 1) + fibonacci(numero - 2);
}

int main() {
    int numero;
    cout << "Digite um numero inteiro para calcular o  Fibonacci: ";
    cin >> numero;

    cout << "O fibonacci é: " << fibonacci(numero);

    return 0;
}