#include <iostream>

using namespace std;

int main() {
    int qntDeSequencia;
    int qntDeNumeros;

    int **sequencias;
    int soma = 0;

    cout << "Digite a quantidade de sequencias: ";
    cin >> qntDeSequencia;

    cout << "Digite a quantidade de numeros dentro de cada sequencia: ";
    cin >> qntDeNumeros;

    sequencias = (int **) malloc(qntDeSequencia * sizeof(int));

    for(int i = 0; i < qntDeSequencia; i++) {
        sequencias[i] = (int *) malloc(qntDeNumeros * sizeof(int));

        cout << "Digite os números da " << i + 1 << " sequência: ";

        for(int j = 0; j < qntDeNumeros; j++) {
            cin >> sequencias[i][j];
        }
    }

    for(int i = 0; i < qntDeSequencia; i++) {
        soma = 0;
        cout << "A soma dos números pares da " << i + 1 << " sequência [";

        for(int j = 0; j < qntDeNumeros; j++) {
            cout << sequencias[i][j];
            if(j != qntDeNumeros - 1) {
                cout << ", ";
            }
            if(sequencias[i][j] % 2 == 0) {
                soma += sequencias[i][j];
            }
        }

        cout << "] é: "<< soma << endl;
    }



    return 0;
}