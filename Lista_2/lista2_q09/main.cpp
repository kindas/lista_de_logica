#include <iostream>
#include <string.h>

using namespace std;

int main() {

    char frase[255];
    char palavra[20];
    int ocorrencias = 0;

    cout << "Digite uma frase com até 255 caracteres: ";
    cin.getline(frase, sizeof(frase));
    cout << "Digite uma palavra com até 20 caracteres: ";
    cin.getline(palavra, sizeof(palavra));

    int tamanhoDaFrase = strlen(frase);
    int tamanhoDaPalavra = strlen(palavra);

    for (int i = 0; i < tamanhoDaFrase; i++) {
        int contido = true;
        for (int j = 0; j < tamanhoDaPalavra; j++) {
            if (palavra[j] == frase[i + j] && contido)
                contido = true;
            else
                contido = false;
        }
        if (contido)
            ocorrencias++;
    }

    cout << "A palavra '" << palavra << "' occore " << ocorrencias << "x na frase '" << frase << "'" << endl;

    return 0;
}