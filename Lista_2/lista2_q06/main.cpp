#include <iostream>

using namespace std;

int main() {
    int m, primeiroNumeroDaSeq;

    printf("Digite o valor de m: ");
    cin >> m;

    primeiroNumeroDaSeq = 1;
    for (int n = 1; n <= m; n++) {
        cout << n << "³ = " << primeiroNumeroDaSeq;
        for (int i = 1; i < n; i++) {
            primeiroNumeroDaSeq += 2;
            cout << "+" << primeiroNumeroDaSeq;
        }
        cout << endl;
        primeiroNumeroDaSeq += 2;
    }

    return 0;
}
